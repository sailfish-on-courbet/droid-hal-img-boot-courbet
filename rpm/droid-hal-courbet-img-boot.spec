%define device courbet

%define mkbootimg_cmd mkbootimg --ramdisk %{initrd}   --kernel %{kernel} --base 0x00000000 --pagesize 4096 --cmdline ‘console=ttyMSM0,115200n8 earlycon=msm_geni_serial,0x880000 androidboot.hardware=qcom androidboot.console=ttyMSM0 androidboot.usbcontroller=a600000.dwc3 service_locator.enable=1 lpm_levels.sleep_disabled=1 loop.max_part=7 androidboot.selinux=permissive audit=0 selinux=0’ --header_version 2 --dtb $ANDROID_ROOT/device/xiaomi/courbet/dtb-img/dtb  --output

%define root_part_label userdata

%define display_brightness_path /sys/class/backlight/panel0-backlight/brightness
%define display_brightness 1024

%define lvm_root_size 5000

%include initrd/droid-hal-device-img-boot.inc
